var width = window.innerWidth
var height = window.innerHeight
var amplitude

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
  background(0,0,0)
}

function preload(){
  sound = loadSound('assets/teknotest.wav');
}


function setup(){
  smooth();
  let cnv = createCanvas(window.innerWidth, window.innderHeight, WEBGL);
  cnv.mouseClicked(togglePlay);
  fft = new p5.FFT();
  fft.smooth(0.85)
  sound.amp(0.2);
  colorMode(HSB, 255)
  angleMode(RADIANS)
  background(0,0,0)
  sound.loop()
}
var t = 0
var hue = 0

function togglePlay() {
  if (sound.isPlaying()) {
    sound.pause();
  } else {
    sound.loop();
  }
}

let default_sat = 195
let default_brightness = 185

function drawBox(hue, sideLength, thickness, rotate){
  stroke((256 + 20*t - hue)%256, 100, 180)
  if(hue >= 256){
    stroke((128+256 + 20*t - hue)%256, 100, 0)
  }
  noFill();
  strokeWeight(thickness)
  rotateX(rotate * map(noise(1000*t), 0, 1, 0.95, 1.05))
  rotateY(rotate * map(noise(1000*t), 0, 1, 0.95, 1.05))
  rotateZ(rotate * map(noise(1000*t), 0, 1, 0.95, 1.05))
  sphere(sideLength, 4, 2)
  // box(sideLength)
  rotateZ(-rotate * map(noise(1000*t), 0, 1, 0.95, 1.05))
  rotateY(-rotate * map(noise(1000*t), 0, 1, 0.95, 1.05))
  rotateX(-rotate * map(noise(1000*t), 0, 1, 0.95, 1.05))
}

var t = 0
let speclen = 64

let revolutions = 1
let horizontal_angle = (TAU * revolutions)/speclen
let vertical_angle = PI/speclen

var hue = 0
function draw() {
  if (t==0){
    windowResized()
    background(0,0,0)
  }
  noiseDetail(4, 0.5)
  if (t==1){
    t = 0
  }
  let spectrum = fft.analyze(speclen);
  let energy = fft.getEnergy(20, 90)

  rotateZ(-0.002*spectrum[0])

  rotateY(-0.003*spectrum[speclen/2])
  rotateX(-0.005*spectrum[speclen-1])

  rotateZ(0.03*t*QUARTER_PI)
  rotateY(0.06*t*QUARTER_PI)
  rotateX(0.09*t*QUARTER_PI)

  for (let i = 0; i< speclen; i++){
    if(noise((2*t-0.2*i)) + 0.005 * (i/speclen) * spectrum[i] > 0.76){
      drawBox(((i/speclen)*256)%256, (i/speclen)*(height/2) + 0.1*spectrum[i], 1, 0.003*spectrum[i])
    }
    else{
      if((1000+2*t-0.2*i) % 8 < 0.1) {
        drawBox(((i/speclen)*256)%256, (i/speclen)*(height/2) + 0.1*spectrum[i], 1, 0.003*spectrum[i])
      }
      else{
        drawBox(295 - (i/speclen)*40 , (i/speclen)*(height/2) + 0.1*spectrum[i], 1, 0.005*spectrum[i])
      }
    }
  }
  t += 0.1
}
